import { defineStore } from '../pinia/index'

export const useStore = defineStore("stroeId", {
    state: () => {
        return {
            name: 'jack chen',
            age: 40
        }
    },
    actions:{
        increment(num){
            //获取state返回对象数据
            console.log(this.age + num)
            return this.age + num
        }
    }
})

