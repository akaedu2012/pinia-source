/**
 * defineStore接受三种传参方式
 * 第一种是传入id+options
 * 第二种是只传入options(而id包含在这个options内部),options是对象
 * 第三种是传入id+setup函数
 */

export function defineStore(idOrOptions, optionsOrSetup) {
    //单独定义id和Options
    let id, options
    //接受和处理第一个参数,id:string,options:object
    if (typeof idOrOptions == "string") {
        id = idOrOptions
        options = optionsOrSetup  //对象或函数
    } else {
        options = idOrOptions //对象
        id = options.id
    }

    //返回函数
    function useStore() {
        let piniaStore = {
            ...options
        }
        return piniaStore
    }

    return useStore //用户使用这个函数就能获取 store实例对象

}
