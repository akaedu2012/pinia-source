import {defineStore} from '../pinia/index'
import { computed,ref } from 'vue'

export const useStore = defineStore("storeId",()=>{
    const age = ref(23)
    const name = "jack"
    function changeCounter(val){
        age.value = val
        return age.value
    }
    /* const changeCount2 = ()=>{

    } */
    const age10 = computed(()=>{
        return age.value + 100
    })

    return {
        age,
        name,
        changeCounter,
        age10
    }
})
